﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

/// <summary>This is the C#-ified version of my command-line application to
/// retrieve commit comments through BitBucket's web API.  Its output follows
/// the same formatting rules as the original Rust version, writing comments
/// sorted by timestamp to a plain-text <c>.txt</c> file in a folder called
/// <c>commit_logs</c>.  Please forgive the state of the documentation; C#
/// is new to me, and the Microsoft .NET guides are less than thorough on
/// the topic.</summary>

namespace SharpBucket
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();
        private static UTF8Encoding uenc = new UTF8Encoding();
        private static readonly string log_directory = ".\\commit_logs";
        static async Task Main(string[] args)
        {
            /// Save reports by time of processing, to the second;
            /// if the log directory does not exist, create it.  If a log
            /// for the current date/time exists, delete it and create new.
            string processTime = DateTime.Now.ToString("yyyy.MM.dd-HH.mm.ss");
            string reportPath = $".\\commit_logs\\{processTime}.txt";
            if (!Directory.Exists(log_directory))
                Directory.CreateDirectory(log_directory);
            if (File.Exists(reportPath))
                File.Delete(reportPath);
            
            var commits = await ProcessCommits();
            using (FileStream fs = File.Create(reportPath))
            {
                string head = $"Commit comments for calanceus/interviews17 as of {DateTime.Now}:\n\n";
                fs.Write(uenc.GetBytes(head), 0, head.Length);
                foreach (var commit in commits.Values)
                {
                    string entry = $"{commit.Date}\t| {commit.Message}\n";
                    fs.Write(uenc.GetBytes(entry), 0, entry.Length);
                }
            }
        }

        /// <summary>Process repository commit logs asynchronously using C#'s
        /// JsonSerializer, which is pickier than I expected.  The formatting
        /// of the log returned by the API is accounted for in <c>commit.cs</c>,
        /// which defines the <c>CommitSet</c> type called here along with a
        /// number of constituent subtypes to parse the JSON into.</summary>
        private static async Task<CommitSet> ProcessCommits()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("User-Agent", "C# Command-Line BitBucket Commit Getter");
            var streamTask = client.GetStreamAsync("https://api.bitbucket.org/2.0/repositories/calanceus/interviews17/commits");
            var commits = await JsonSerializer.DeserializeAsync<CommitSet>(await streamTask);
            return commits;
        }
    }
}
