using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

/// The classes in this file describe distinct objects represented by the JSON
/// text of the BitBucket commit log.  Is this a more laborious path to
/// satisfying the C# JsonSerializer than strictly necessary?  Perhaps.  But
/// it was edifying to write, and might be useful in extending the project
/// later.

namespace SharpBucket
{
    public class CommitSet
    {
        [JsonPropertyName("pagelen")]
        public int PageLen { set; get; }

        [JsonPropertyName("values")]
        public List<Commit> Values { set; get; }
    }

    public class Commit
    {
        [JsonPropertyName("rendered")]
        public MessageObject Rendered { set; get; }

        [JsonPropertyName("hash")]
        public string Hash { set; get; }

        [JsonPropertyName("repository")]
        public RepoSet Repository { set; get; }

        [JsonPropertyName("links")]
        public LinkSet Links { set; get; }

        [JsonPropertyName("author")]
        public CommitAuthor Author { set; get; }

        [JsonPropertyName("summary")]
        public MessageObject Summary { set; get; }

        [JsonPropertyName("parents")]
        public List<CommitLink> Parents { set; get; }

        [JsonPropertyName("date")]
        public DateTime Date { set; get; }

        [JsonPropertyName("message")]
        public string Message { set; get; }

        [JsonPropertyName("type")]
        public string Type { set; get; }
    }

    public class MessageObject
    {
        [JsonPropertyName("raw")]
        public string Raw { set; get; }

        [JsonPropertyName("markup")]
        public string Markup { set; get; }

        [JsonPropertyName("html")]
        public string Html { set; get; }

        [JsonPropertyName("type")]
        public string Type { set; get; }
    }

    public class RepoSet
    {
        [JsonPropertyName("links")]
        public LinkSet Links { set; get; }

        [JsonPropertyName("type")]
        public string Type { set; get; }

        [JsonPropertyName("name")]
        public string Name { set; get; }

        [JsonPropertyName("full_name")]
        public string FullName { set; get; }

        [JsonPropertyName("uuid")]
        public string Uuid { set; get; }
    }

    public class LinkSet
    {
        [JsonPropertyName("self")]
        public Link Self { set; get; }

        [JsonPropertyName("html")]
        public Link Html { set; get; }

        [JsonPropertyName("avatar")]
        public Link Avatar { set; get; }

        [JsonPropertyName("comments")]
        public Link Comments { set; get; }

        [JsonPropertyName("patch")]
        public Link Patch { set; get; }

        [JsonPropertyName("diff")]
        public Link Diff { set; get; }

        [JsonPropertyName("approve")]
        public Link Approve { set; get; }

        [JsonPropertyName("statuses")]
        public Link Statuses { set; get; }
    }

    public class Link
    {
        [JsonPropertyName("href")]
        public Uri Href { set; get; }
    }

    public class CommitAuthor
    {
        [JsonPropertyName("raw")]
        public string Raw { set; get; }

        [JsonPropertyName("type")]
        public string Type { set; get; }

        [JsonPropertyName("user")]
        public BBUser User { set; get; }
    }

    public class BBUser
    {
        [JsonPropertyName("display_name")]
        public string DisplayName { set; get; }

        [JsonPropertyName("uuid")]
        public string Uuid { set; get; }

        [JsonPropertyName("links")]
        public LinkSet Links { set; get; }

        [JsonPropertyName("nickname")]
        public string Nickname { set; get; }

        [JsonPropertyName("type")]
        public string Type { set; get; }

        [JsonPropertyName("account_id")]
        public string AccountId { set; get; }
    }

    public class CommitLink
    {
        [JsonPropertyName("hash")]
        public string Hash { set; get; }

        [JsonPropertyName("type")]
        public string Type { set; get; }

        [JsonPropertyName("links")]
        public LinkSet Links { set; get; }
    }
}