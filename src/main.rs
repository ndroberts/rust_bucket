//! # RustBucket
//! This is a small command-line application written as part of a coding
//! interview challenge.  It uses BitBucket's public web API to retrieve
//! all the commit comments from a particular repository.  It currently
//! defaults to the repo specific to the interview challenge, but also
//! accepts command-line arguments to target different repositories.

use std::error::Error;
use std::fs::{create_dir, read_dir, File};
use std::io::{prelude::*, BufWriter};
use std::path::Path;
use std::result::Result;
use std::vec::Vec;

use chrono::prelude::*;
use json::parse as parse_json;
use reqwest::{Client, Url};


#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let run_args: Vec<String> = std::env::args().collect();
    let mut org: String = String::from("calanceus");
    let mut repo: String = String::from("interviews17");
    for i in 0..run_args.len() {
        match &run_args[i][..] {
            "-h" => print_help(),
            "-o" => org = String::from(&run_args[i + 1]),
            "-r" => repo = String::from(&run_args[i + 1]),
            _ => continue,
        }
    }
    let repo_id: String = format!("{}/{}", org, repo);
    let target: String = format!(
        "https://api.bitbucket.org/2.0/repositories/{}/",
        repo_id
    );
    let default_fields = String::from("commits?fields=values.date,values.message");
    let target_string = format!("{}{}", target, default_fields);
    let uri = Url::parse(&target_string)?;
    let current_time: DateTime<Local> = Local::now();
    let commits = request_commits(uri).await?;

    generate_report(repo_id, current_time.to_string(), &commits)?;
    println!("Request completed at {}", current_time);

    Ok(())
}

/// Creates and sends the HTTP request to the BitBucket API.
/// Returns commit comments as a vector of Strings.
async fn request_commits(repo: Url) -> Result<Vec<String>, Box<dyn Error>> {
    let http_client = Client::new();
    let request = http_client.get(repo).send().await?;
    let mut result = Vec::new();
    // EXTEND: Maybe do this 'status' as a match statement, to account for different status returns?
    println!("Status: {}", request.status());
    let body = request.text().await?;
    let json_object = parse_json(&body)?;
    for (_, val) in json_object.entries() {
        for obj in val.members() {
            let entry = format!("{}\t| {}\n", obj["date"], obj["message"]);
            result.push(entry);
        }
    }

    Ok(result)
}

/// Writes the retrieved comments to a text file.  Files are named by
/// request time, and stored in the "commit_logs" folder.
fn generate_report(repo: String, time: String, data: &Vec<String>) -> Result<(), Box<dyn Error>> {
    let path = Path::new("./commit_logs");
    let report_dir = read_dir(path);
    match report_dir {
        Ok(_) => println!("Writing report to './commit_logs/'..."),
        Err(_) => {
            create_dir(path)?;
            println!("Creating directory './commit_logs/'...");
        }
    }
    let trunc_index = &time.find(".").unwrap_or(19);
    let mut file_name = time.split_at(*trunc_index).0.to_string();
    file_name = format!("./commit_logs/{}.txt", &file_name)
        .replace(" ", "_")
        .replace(":", ".");
    let output = File::create(&file_name)?;
    let header = format!("Git commit comments for {}, as of {}:\n\n", repo, time);
    let mut buffer = BufWriter::new(output);
    buffer.write(header.as_bytes())?;
    for line in data {
        buffer.write(&line.as_bytes())?;
    }
    buffer.flush()?;

    Ok(())
}

/// Simple explanation of command-line argument use.
/// To be filled in as argument options are implemented.
fn print_help() {
    // TODO: Simple instructions for CL args
}

// |-------------------------------------------------------------------------|
// |                             Unit Tests                                  |
// |-------------------------------------------------------------------------|

#[cfg(test)]
mod tests{
    use crate::request_commits;
    use std::io::{Read, Write};
    use std::net::TcpStream;
    use mockito::{mock, server_url};
    use reqwest::Url;

    #[test]
    fn test_request() {
        // let default_fields = String::from("commits?fields=values.date,values.message");
        // let fuckin_string = format!("{}/{}/{}", "anorg", "arepo", &default_fields);
        let mock_request = mock("GET", "/anorg/arepo/commits?fields=values.date,values.message").create();
            // .with_status(200)
            // .with_header("content-type", "application/json")
            // .create();

        let mut tcp_stream = TcpStream::connect(server_url()).unwrap();
        tcp_stream.write_all("GET /anorg/arepo/commits?fields=values.date,values.message HTTP/1.1\r\n\r\n".as_bytes()).unwrap();
        // let test_target = server_url() + "/anorg/arepo/commits?fields=values.date,values.message";
        // let test_url = Url::parse(&test_target).unwrap();
        // let _commits = request_commits(test_url);
        let mut response = String::new();
        tcp_stream.read_to_string(&mut response).unwrap();
        tcp_stream.flush().unwrap();

        mock_request.assert();
    }
}