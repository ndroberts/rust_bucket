# README #

### What is this repository for? ###

* A command-line application that interfaces with the BitBucket web API
* 0.1.0

At present, this application retrieves Git comments from a specified BitBucket
repository, and stores them in a text file.  

### Why Rust? ###
The challenge, as presented to me, allowed for "any language" I chose.  I haven
no doubt this could have been done fairly trivially in Python or Java, both of
which I know a bit, or perhaps C or Perl, with which I am less familiar.  I've
developed quite a fondness for the Rust language over the past six months, as 
it is a versatile language that produces relatively performant executables with 
greater memory safety than many other languages.  It assures that memory safety 
through a positively draconian borrow-checker at compile time, but honestly, I 
think it encourages me to use better variable hygiene.  Also, maybe I'm a glutton
for punishment, but that's neither here nor there.

### How do I get set up? ###

The `/bin/` folder contains pre-compiled binaries for Windows and Linux systems.
Each should be runnable independently, and should produce its output in the
directory from which it is run.

You can also compile and run the code from source by using Rust's Cargo build
tool.  Instructions for installing the Rust toolchain can be found on the
official Rust-Lang site at [https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install).
With Rust installed, from the project's root directory, use the command

	`$ cargo run`

to compile and execute the code in development mode.

I'm still working on proper unit testing, to be honest; I haven't done it with
HTTP requests in Rust before.  Thus, while the command

	`$ cargo test`

can be used to execute the program in test configuration, it is known not to
pass at the moment.

### What's this 'SharpBucket' business? ###

As part of the coding challenge, I was asked to re-write my original Rust
application in C# for the .NET platform.  That's what that is.  I'm new to C#,
but it was fun to try, and I think it works as intended.
